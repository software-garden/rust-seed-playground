include ./Makefile.d/defaults.mk

all: ## Build the site and run unit tests (DEFAULT)
all: dist test
.PHONY: all

test: dist
	cargo test
.PHONY: test

dist: ## Prepare the content and store in the dist/ directory.
dist: $(shell find src/)
dist: base-url ?= "/"
dist:
	mkdir --parents $@
	trunk build --public-url=$(base-url)
	touch $@

install: ## Install the preapred content in the ${prefix} directory
install: prefix ?= $(out)
install: dist
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive dist/* $(prefix)
.PHONY: install


### DEVELOPMENT

develop: ## Watch, rebuild and serve the content
develop:
	trunk serve
.PHONY: develop

serve: ## Serve the prepared content
serve: dist
serve:
	miniserve --index=index.html dist/
.PHONY: serve

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX \
		--exclude='!.envrc.private'
.PHONY: clean


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)


### HELP

help: ## Print this help message
help: # TODO: Handle section headers in awk script
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help
