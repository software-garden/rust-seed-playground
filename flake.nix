{
  description = "The Rust Trunk playground";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, flake-compat }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        shared-inputs = [
            pkgs.git
            pkgs.gnumake
            pkgs.rustup
            pkgs.gcc
            pkgs.openssl
            pkgs.pkg-config
            pkgs.trunk
        ];
      in rec {
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "rust-trunk-playground";
          src = self;
          buildInputs = shared-inputs ++ [
          ];
        };

        devShell = pkgs.mkShell {
          name = "rust-trunk-playground-development-shell";
          src = self;
          buildInputs = shared-inputs ++ [
            pkgs.cachix
            pkgs.miniserve
            pkgs.jq
            pkgs.httpie
            pkgs.rust-analyzer
          ];
        };

      }
    );
}
